/**
 * 
 */
package com.example.message.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 *  @author 754134
 *	App : message
 *
 *	File : ErrorDetails.java
 *
 */
public class ErrorDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6056904202804456721L;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	
	private String details;
	
	private String message;
	
	

	/**
	 * @param details
	 * @param message
	 */
	public ErrorDetails(String details, String message) {
		super();
		this.details = details;
		this.message = message;
		this.timestamp = LocalDateTime.now();
	}

	/**
	 * @return the timestamp
	 */
	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}	
	
	

}

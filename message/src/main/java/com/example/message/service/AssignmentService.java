/*
 * Copyright (c) 2019 Tata Consultancy Services , 
 * 
 *	App : message
 *
 *	File : AssignmentService.java
 * 
 */
package com.example.message.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.example.message.beans.BalanceUpdate;
import com.example.message.beans.Payment;

/**
 * The Class AssignmentService.
 *
 * @author 754134
 * 
 *         File : AssignmentService.java
 */
@Component
public class AssignmentService {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentService.class);
	
	/**
	 * Gets the payments.
	 *
	 * @param filePath
	 *            the file path
	 * @return the payments
	 */
	public List<Payment> getPayments(String filePath){
		
		List<BalanceUpdate> list = processInputFile(filePath);
		
		List<Payment> payments = new ArrayList<>();
		for(int i=0;i < list.size()-1;i++){
			if(list.get(i+1).getCreditAmount() != list.get(i).getCreditAmount()){
				payments.add(new Payment((list.get(i).getCreditAmount().subtract(list.get(i+1).getCreditAmount())).abs()));
			}
		}
		
		payments.forEach(e->LOGGER.info(e.toString()));
		
		return payments;
	}

	/**
	 * Process input file.
	 *
	 * @param inputFilePath
	 *            the input file path
	 * @return the list
	 */
	public List<BalanceUpdate> processInputFile(String inputFilePath) {
	    List<BalanceUpdate> balanceUpdates = new ArrayList<BalanceUpdate>();
	    try{
	      balanceUpdates = Files.newBufferedReader(Paths.get(inputFilePath)).lines().skip(1).map(mapToBalanceUpdate).collect(Collectors.toList());
	    } catch (IOException e) {
	    	LOGGER.error(e.getMessage());
	    }
	    return balanceUpdates ;
	}
	
	/** The map to item. */
	private Function<String, BalanceUpdate> mapToBalanceUpdate = (line) -> {
		  String[] temp = line.split(",");
		  BalanceUpdate balanceUpdate = new BalanceUpdate();
		  
		  if (temp.length == 4) {
			  balanceUpdate.setTimestamp(LocalDateTime.parse(temp[3]));
			  balanceUpdate.setCreditAmount(new BigDecimal(temp[0]));
			  balanceUpdate.setDebitAmount(new BigDecimal(temp[1]));
			  balanceUpdate.setNetAmount(new BigDecimal(temp[2]));
		  } else {
			  LOGGER.info("Skipped : " + temp);
		  }
		  return balanceUpdate;
		};
}

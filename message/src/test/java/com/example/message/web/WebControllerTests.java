/**
 * 
 */
package com.example.message.web;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.message.beans.BalanceUpdate;
import com.example.message.beans.Payment;
import com.example.message.service.AssignmentService;

/**
 *  @author 754134
 *  
 *	App : message
 *
 *	File : WebControllerTests.java
 *
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class WebControllerTests {

	@Value("${application.filepath}")
	private String filepath;
	@Autowired
	private AssignmentService assignmentService;
	
	@Test
	public void getPayments(){
		List<Payment> payments = null; 
		payments = assignmentService.getPayments(filepath);
		
		Assert.assertNotNull(payments);
		assertEquals(3, payments.size());
		
	}
	
	@Test
	public void fileData(){
		List<BalanceUpdate> balanceUpdates = null;
		balanceUpdates = assignmentService.processInputFile(filepath);
		Assert.assertNotNull(balanceUpdates);
		
	}

}

BalanceUpdates.css is provided with multiple balance updates

1. Task is to read the file and generate an artificial payments message with payment amount equal to the difference of two credit records when there is difference in subsequent balance updates 

- Input file is a csv file
- Input file path to be configured in application.properties

- Get service : http://{ip}:{port}/test/payments is exposed to get the results (8085 port is configured)

##Test
- case to check BalanceUpdates are not empty
- case to check the number of payment messages

/*
 *	App : message
 *
 *	File : WebController.java
 * 
 */
package com.example.message.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.message.beans.Payment;
import com.example.message.service.AssignmentService;

/**
 *  @author 754134
 *	App : message
 *
 *	File : WebController.java
 *
 */
@RestController
@RequestMapping("/test")
public class WebController {
	
	@Value("${application.filepath}")
	private String inputFilePath;
	
	@Autowired
	private AssignmentService assignmentService;

	/**
	 * Gets the payments.
	 *
	 * @return the payments
	 */
	@GetMapping("/payments")
	public List<Payment> getPayments(){
		return assignmentService.getPayments(inputFilePath);
	}
}
